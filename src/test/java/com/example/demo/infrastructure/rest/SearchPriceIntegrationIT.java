package com.example.demo.infrastructure.rest;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import com.example.demo.Practica1ResueltaApplication;
import com.example.demo.application.search_price.SearchPriceResponse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Practica1ResueltaApplication.class)
public class SearchPriceIntegrationIT {

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void should_Works_First_Case_Date2020_06_14_10_00_00_ProductId_35455_BrandId_1() {
		
		ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity("/price/search?date=2020-06-14-10.00.00&productId=35455&brandId=1", SearchPriceResponse.class);
	
		assertTrue(res.getStatusCode().is2xxSuccessful());
		
		SearchPriceResponse response = res.getBody();
		
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-14-00.00.00", response.getDateStart());
		assertEquals("2020-12-31-23.59.59", response.getDateEnd());
		assertEquals("1", response.getRate());
		assertEquals("35,50 EUR", response.getPrice());
	}
	
	@Test
	public void should_Works_Second_Case_Date2020_06_14_16_00_00_ProductId_35455_BrandId_1() {
		
		ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity("/price/search?date=2020-06-14-16.00.00&productId=35455&brandId=1", SearchPriceResponse.class);
		
		assertTrue(res.getStatusCode().is2xxSuccessful());
		
		SearchPriceResponse response = res.getBody();
		
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-14-15.00.00", response.getDateStart());
		assertEquals("2020-06-14-18.30.00", response.getDateEnd());
		assertEquals("2", response.getRate());
		assertEquals("25,45 EUR", response.getPrice());
	}
	
	@Test
	public void should_Works_Third_Case_Date2020_06_14_21_00_00_ProductId_35455_BrandId_1() {
		
		ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity("/price/search?date=2020-06-14-21.00.00&productId=35455&brandId=1", SearchPriceResponse.class);
		
		assertTrue(res.getStatusCode().is2xxSuccessful());
		
		SearchPriceResponse response = res.getBody();
		
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-14-00.00.00", response.getDateStart());
		assertEquals("2020-12-31-23.59.59", response.getDateEnd());
		assertEquals("1", response.getRate());
		assertEquals("35,50 EUR", response.getPrice());
	}
	
	@Test
	public void should_Works_Fourth_Case_Date2020_06_15_10_00_00_ProductId_35455_BrandId_1() {
		
		ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity("/price/search?date=2020-06-15-10.00.00&productId=35455&brandId=1", SearchPriceResponse.class);
		
		assertTrue(res.getStatusCode().is2xxSuccessful());
		
		SearchPriceResponse response = res.getBody();
		
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-15-00.00.00", response.getDateStart());
		assertEquals("2020-06-15-11.00.00", response.getDateEnd());
		assertEquals("3", response.getRate());
		assertEquals("30,50 EUR", response.getPrice());
	}
	
	@Test
	public void should_Works_Fifth_Case_Date2020_06_16_21_00_00_ProductId_35455_BrandId_1() {
		
		ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity("/price/search?date=2020-06-16-21.00.00&productId=35455&brandId=1", SearchPriceResponse.class);
		
		assertTrue(res.getStatusCode().is2xxSuccessful());
		
		SearchPriceResponse response = res.getBody();
		
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-15-16.00.00", response.getDateStart());
		assertEquals("2020-12-31-23.59.59", response.getDateEnd());
		assertEquals("4", response.getRate());
		assertEquals("38,95 EUR", response.getPrice());
	}
	
	@Test
	public void should_Not_Works_Sixth_Case_Date2020_06_16_21_00_00_ProductId_3555_BrandId_2() {
		
		ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity("/price/search?date=2020-06-16-21.00.00&productId=3555&brandId=2", SearchPriceResponse.class);
		
		assertTrue(res.getStatusCode().is4xxClientError());
		
		SearchPriceResponse response = res.getBody();
		
		assertEquals(null, response.getProductId());
		assertEquals(null, response.getBrandId());
		assertEquals(null, response.getDateStart());
		assertEquals(null, response.getDateEnd());
		assertEquals(null, response.getRate());
		assertEquals(null, response.getPrice());
	}
	
	@Test
	public void should_Not_Works_Seventh_Case_Date2020_06_16_21_00_00_ProductId_35455_BrandId_1_Bad_Formed_Date() {
		
		ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity("/price/search?date=2020-06-16 21.00.00&productId=35455&brandId=1", SearchPriceResponse.class);
		
		assertTrue(res.getStatusCode().is4xxClientError());
		
		SearchPriceResponse response = res.getBody();
		
		assertEquals(null, response.getProductId());
		assertEquals(null, response.getBrandId());
		assertEquals(null, response.getDateStart());
		assertEquals(null, response.getDateEnd());
		assertEquals(null, response.getRate());
		assertEquals(null, response.getPrice());
	}
}
