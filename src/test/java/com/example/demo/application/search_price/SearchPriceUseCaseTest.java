package com.example.demo.application.search_price;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.example.demo.domain.entities.Price;
import com.example.demo.domain.exceptions.PriceNotFoundException;
import com.example.demo.domain.respositories.PriceRepository;
import com.example.demo.domain.services.PriceService;
import com.example.demo.domain.util.FormatUtil;
import com.example.demo.domain.vos.Amount;
import com.example.demo.domain.vos.Currency;

public class SearchPriceUseCaseTest {

	private PriceRepository priceRepository = Mockito.mock(PriceRepository.class);
	private final PriceService priceService = new PriceService(priceRepository);
	private final SearchPriceUseCase searchPriceUseCase = new SearchPriceUseCase(priceService);
		
	@Test
	public void should_Search_Price_First_Case_Date2020_06_14_10_00_00_ProductId_35455_BrandId_1() throws PriceNotFoundException {
		
		Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());
		
		SearchPriceRequest request = SearchPriceRequest.builder()
																.date(FormatUtil.dateParse("2020-06-14-10.00.00"))
																.productId("35455")
																.brandId("1")
																.build();
		
		SearchPriceResponse response = searchPriceUseCase.execute(request);
					
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-14-00.00.00", response.getDateStart());
		assertEquals("2020-12-31-23.59.59", response.getDateEnd());
		assertEquals("1", response.getRate());
		assertEquals("35,50 EUR", response.getPrice());
	}
	
	@Test
	public void should_Search_Price_Second_Case_Date2020_06_14_16_00_00_ProductId_35455_BrandId_1() throws PriceNotFoundException {
		
		Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());
		
		SearchPriceRequest request = SearchPriceRequest.builder()
																.date(FormatUtil.dateParse("2020-06-14-16.00.00"))
																.productId("35455")
																.brandId("1")
																.build();
		
		SearchPriceResponse response = searchPriceUseCase.execute(request);
			
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-14-15.00.00", response.getDateStart());
		assertEquals("2020-06-14-18.30.00", response.getDateEnd());
		assertEquals("2", response.getRate());
		assertEquals("25,45 EUR", response.getPrice());
	}
	
	@Test
	public void should_Search_Price_Third_Case_Date2020_06_14_21_00_00_ProductId_35455_BrandId_1() throws PriceNotFoundException {
		
		Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());
		
		SearchPriceRequest request = SearchPriceRequest.builder()
																.date(FormatUtil.dateParse("2020-06-14-21.00.00"))
																.productId("35455")
																.brandId("1")
																.build();
		
		SearchPriceResponse response = searchPriceUseCase.execute(request);
			
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-14-00.00.00", response.getDateStart());
		assertEquals("2020-12-31-23.59.59", response.getDateEnd());
		assertEquals("1", response.getRate());
		assertEquals("35,50 EUR", response.getPrice());
	}
	
	@Test
	public void should_Search_Price_Fourth_Case_Date2020_06_15_10_00_00_ProductId_35455_BrandId_1() throws PriceNotFoundException {
		
		Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());
		
		SearchPriceRequest request = SearchPriceRequest.builder()
																.date(FormatUtil.dateParse("2020-06-15-10.00.00"))
																.productId("35455")
																.brandId("1")
																.build();
		
		SearchPriceResponse response = searchPriceUseCase.execute(request);
			
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-15-00.00.00", response.getDateStart());
		assertEquals("2020-06-15-11.00.00", response.getDateEnd());
		assertEquals("3", response.getRate());
		assertEquals("30,50 EUR", response.getPrice());
	}
	
	@Test
	public void should_Search_Price_Fifth_Case_Date2020_06_16_21_00_00_ProductId_35455_BrandId_1() throws PriceNotFoundException {
		
		Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());
		
		SearchPriceRequest request = SearchPriceRequest.builder()
																.date(FormatUtil.dateParse("2020-06-16-21.00.00"))
																.productId("35455")
																.brandId("1")
																.build();
		
		SearchPriceResponse response = searchPriceUseCase.execute(request);
			
		assertEquals("35455", response.getProductId());
		assertEquals("1", response.getBrandId());
		assertEquals("2020-06-15-16.00.00", response.getDateStart());
		assertEquals("2020-12-31-23.59.59", response.getDateEnd());
		assertEquals("4", response.getRate());
		assertEquals("38,95 EUR", response.getPrice());
	}
	
	@Test
	public void should_Search_Price_Not_Found_Sixth_Case_Date2020_06_16_21_00_00_ProductId_3545_BrandId_2(){
		
		Mockito.when(priceRepository.findAll()).thenReturn(this.getPrices());
		
		SearchPriceRequest request = SearchPriceRequest.builder()
																.date(FormatUtil.dateParse("2020-06-16-21.00.00"))
																.productId("3545")
																.brandId("2")
																.build();
		try {
			searchPriceUseCase.execute(request);
		} catch (PriceNotFoundException e) {
			assertEquals(e.getClass(), PriceNotFoundException.class);
		}
	}
		
	private List<Price> getPrices(){
		List<Price> listPrices = new ArrayList<>();
		
		listPrices.add(Price.builder().brandId("1").startDate(FormatUtil.dateParse("2020-06-14-00.00.00"))
				.endDate(FormatUtil.dateParse("2020-12-31-23.59.59")).priceList(1).productId("35455")
				.priority(0).amount(new Amount(Double.valueOf(35.50))).currency(Currency.valueOf("EUR")).build());
		
		listPrices.add(Price.builder().brandId("1").startDate(FormatUtil.dateParse("2020-06-14-15.00.00"))
				.endDate(FormatUtil.dateParse("2020-06-14-18.30.00")).priceList(2).productId("35455")
				.priority(1).amount(new Amount(Double.valueOf(25.45))).currency(Currency.valueOf("EUR")).build());
		
		listPrices.add(Price.builder().brandId("1").startDate(FormatUtil.dateParse("2020-06-15-00.00.00"))
				.endDate(FormatUtil.dateParse("2020-06-15-11.00.00")).priceList(3).productId("35455")
				.priority(1).amount(new Amount(Double.valueOf(30.50))).currency(Currency.valueOf("EUR")).build());
		
		listPrices.add(Price.builder().brandId("1").startDate(FormatUtil.dateParse("2020-06-15-16.00.00"))
				.endDate(FormatUtil.dateParse("2020-12-31-23.59.59")).priceList(4).productId("35455")
				.priority(1).amount(new Amount(Double.valueOf(38.95))).currency(Currency.valueOf("EUR")).build());
		
		return listPrices;
	}
}
