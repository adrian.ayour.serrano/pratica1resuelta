package com.example.demo.infrastructure.persistence;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.application.mappers.PriceMapper;
import com.example.demo.domain.entities.Price;
import com.example.demo.domain.respositories.PriceRepository;

@Repository
public class PriceRepositoryJPAAdapter implements PriceRepository {

	@Autowired
	PriceRepositoryJPA priceRepositoryJPA;
	
	@Override
	public List<Price> findAll() {
		
		return priceRepositoryJPA.findAll()
											.stream()
											.map(PriceMapper::toModel).collect(Collectors.toList());
	}

}
