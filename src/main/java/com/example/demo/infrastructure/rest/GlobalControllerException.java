package com.example.demo.infrastructure.rest;

import java.time.format.DateTimeParseException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.demo.domain.exceptions.PriceNotFoundException;


@ControllerAdvice
public class GlobalControllerException {

	@ExceptionHandler
	public ResponseEntity<ErrorDTO> handlePriceNotFoundException(PriceNotFoundException priceNotFound){
		ErrorDTO errorDTO = ErrorDTO.builder()
											.errorMessage(priceNotFound.getMessage())
											.statusCode(HttpStatus.NOT_FOUND.value())
											.timeStamp(System.currentTimeMillis())
											.build();
		return new ResponseEntity<ErrorDTO>(errorDTO, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<ErrorDTO> handleException(DateTimeParseException parseEx){
		ErrorDTO errorDTO = ErrorDTO.builder()
											.errorMessage("Fecha mal formada. (YYYY-mm-dd-HH.mm.ss)")
											.statusCode(HttpStatus.BAD_REQUEST.value())
											.timeStamp(System.currentTimeMillis())
											.build();
		return new ResponseEntity<ErrorDTO>(errorDTO, HttpStatus.BAD_REQUEST);
	}
	
}
