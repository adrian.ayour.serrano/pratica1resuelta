package com.example.demo.infrastructure.rest;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorDTO {

	private String errorMessage;
	private Integer statusCode;
	private Long timeStamp;
}
