package com.example.demo.domain.entities;

import java.time.LocalDateTime;

import com.example.demo.domain.util.FormatUtil;
import com.example.demo.domain.vos.Amount;
import com.example.demo.domain.vos.Currency;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Price {

	private final String brandId;
	private final LocalDateTime startDate;
	private final LocalDateTime endDate;
	private final Integer priceList;
	private final String productId;
	private final Integer priority;
	private final Amount amount;
	private final Currency currency;
	
	
	public String getFinalPrice() {
		return FormatUtil.formatDouble(this.amount.getValue()) + " " + this.currency.getValue();
	}
}
