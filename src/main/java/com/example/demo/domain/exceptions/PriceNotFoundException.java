package com.example.demo.domain.exceptions;

public class PriceNotFoundException extends Exception{

	private static final long serialVersionUID = -2695111949100125174L;

	public PriceNotFoundException(String msg) {
		super(msg);
	}
	
}
