package com.example.demo.domain.respositories;

import java.util.List;

import com.example.demo.domain.entities.Price;

public interface PriceRepository{

	public List<Price> findAll();
	
}
