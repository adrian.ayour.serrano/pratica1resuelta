package com.example.demo.domain.services;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Optional;

import com.example.demo.domain.entities.Price;
import com.example.demo.domain.exceptions.PriceNotFoundException;
import com.example.demo.domain.respositories.PriceRepository;

public class PriceService {

	private PriceRepository priceRepository;
	
	public PriceService(PriceRepository priceRepository) {
		this.priceRepository = priceRepository;
	}
	
	public Price searchPrice(LocalDateTime date, String productId, String brandId) throws PriceNotFoundException {
		
		Optional<Price> price = priceRepository.findAll().stream()
																.filter(p -> p.getStartDate().isBefore(date) && p.getEndDate().isAfter(date))
																.filter(p -> p.getProductId().equals(productId))
																.filter(p -> p.getBrandId().equals(brandId))
																.max(Comparator.comparingInt(Price::getPriority));
		
		if(!price.isPresent()) {
			throw new PriceNotFoundException("Precio no encontrado");
		}
		
		return price.get();
	}
	
}
