package com.example.demo.application.search_price;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchPriceRequest {

	private final String productId;
	private final LocalDateTime date;
	private final String brandId;
	
	
}
