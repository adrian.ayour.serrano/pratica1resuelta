package com.example.demo.application.mappers;

import com.example.demo.domain.entities.Price;
import com.example.demo.domain.vos.Amount;
import com.example.demo.domain.vos.Currency;
import com.example.demo.infrastructure.persistence.PriceJPA;

public class PriceMapper {
	
	private PriceMapper() {}

	public static Price toModel(PriceJPA priceJPA) {
		return Price.builder()
							.amount(new Amount(priceJPA.getPrice()))
							.brandId(priceJPA.getBrandId())
							.currency(Currency.valueOf(priceJPA.getCurrency()))
							.endDate(priceJPA.getEndDate())
							.startDate(priceJPA.getStartDate())
							.priceList(Integer.parseInt(String.valueOf(priceJPA.getPriceList())))
							.priority(priceJPA.getPriority())
							.productId(priceJPA.getProductId())
							.build();
	}
	
}
