# pratica1Resuelta
API Rest application which answer to product petitions with the next format "/price/search?date=2020-06-14-10.00.00&productId=35455&brandId=1";
The application only works with the next date format: "YYYY-mm-dd-HH.mm.ss" .
For running the application you need to run a docker-compose up in the folder you have the project.

Java version: 17
Docker-compose: v2.16.0